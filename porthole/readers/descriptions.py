#!/usr/bin/env python3

"""
    Porthole Reader Class: Decription Reader

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Brian Dolbec,
    Daniel G. Taylor and Wm. F. Wheeler, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import logging
from porthole.readers.commonreader import CommonReader

logger = logging.getLogger(__name__)


class DescriptionReader(CommonReader):
    """ Read and store package descriptions for searching """

    def __init__(self, packages):
        """ Initialize """
        CommonReader.__init__(self)
        self.packages = packages
        self.descriptions = {}

    def run(self):
        """ Load all descriptions """
        # started by load_descriptions() in database.py
        logger.debug("READERS: DescriptionReader(); process id = %d *****************", os.getpid())
        for name, package in self.packages:
            # thread stop check
            if self.cancelled:
                self.done = True
                return
            self.descriptions[name] = package.get_description()
            if not self.descriptions[name]:
                logger.debug("READERS: DescriptionReader(); No description for %s", name)
            self.count += 1

        # flag thread as done
        self.done = True
        logger.debug("READERS: DescriptionReader(); Done")
