#!/usr/bin/env python3

"""
    File: porthole/views/changelog.py
    This file is part of the Porthole, a graphical portage-frontend.

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2009 Brian Dolbec <dol-sen>
    This is free software.  You may redistribute copies of it under the terms of
    the GNU General Public License version 2.
    There is NO WARRANTY, to the extent permitted by law.

    Written by, Brian Dolbec <dol-sen@users.sourceforge.net>

    28 June 20 - Note:
    Gentoo Archives: gentoo-dev-announce
    https://archives.gentoo.org/gentoo-dev-announce/message/651feb859ae9669dfeaa19547fa698dc
    Not sooner than 2016/Nov/15, ChangeLogs will be removed from the
    'gentoo-portage' rsync module. They may or may not still be referenced
    by the thick Manifests.

    https://gitweb.gentoo.org/repo/gentoo.git/atom/gnome-base/gnome-shell?h=master

    Note!!!!!  as of 64 July 2020 porthole depends on beautifulsoup 4, for changelog
    notebook tab
"""

import re
from pathlib import Path
import logging
import gi
gi.require_version('Gtk', '3.0')
from porthole.views.list import ListView
from porthole.views.markup import MarkupView
from porthole.backends import portagelib as portage_lib

logger = logging.getLogger(__name__)
logger.debug("CHANGELOG: id initialized")


class ChangeLogView(ListView, MarkupView):
    """ChangeLog subclass which adds bug# highlighting and opening in
    the defined webbrowser

    example use:
    from porthole.views.changelog import ChangeLogView
    myview = ChangeLogView()
    myview.update(ebuild)

    """
    logger.debug("CHANGELOG: running")
    bug_re = re.compile(r'\d{4,}')
    atom_re = re.compile(r'\S+-\d+\S+')  # was re.compile(r'\S+\-\d+\S+')
    word_re = [atom_re, bug_re]
    word_fn = ['atom', 'bug']
    new_ver_re = re.compile(r'(?P<atom>\*.*) (?P<date>\(.*\))')
    update_re = re.compile(r'(?P<date>\d\d [A-Z][a-z][a-z] \d\d\d\d;) (?P<developer>.*[<]\S+[>]) (?P<text>.*)')
    # could probably tweak the code to not require the update_re and use update_re2 and search the
    # remaining text for atoms, bug's
    update_re2 = re.compile(r'(?P<date>\d\d [A-Z][a-z][a-z] \d\d\d\d;) (?P<developer>.*[<]\S+[>])')
    all = re.compile(r'(?P<text>.*)')
    re_list = [new_ver_re, update_re, update_re2, all]
    re_fn = ['new_ver', 'update', 'update2', 'all']

    def __init__(self):

        ListView.__init__(self, self._get_fn)
        MarkupView.__init__(self)

        self.bugzilla_url = "http://bugs.gentoo.org/show_bug.cgi?id="
        self.bugs = {}
        self.bug_id = 0

        self.changeurl = None
        self.chtext = None
        self.indent = ' ' * 4

    def _get_fn(self, cpv):
        """ The changelog should be written or is being written
        Was: Returns a path to the specified category/package-version ChangeLog
        Over the years, change logs was removed as a file, so we have to go out and
        grab the webpage.

        Now, this might not be the best way to do things but I really do not
        like how python multi processing works. So here's how it works, as
        soon as the package is clicked ChangeLogProcess (changeload.py) uses
        a worker thread to grab the change page and save it in the /tmp dir
        in the form category-name_package-name.porthole and then ends.

        This is all in hopes that the page is ready when the user clicks
        the Changelog tab. If it is not, this function waits until the
        token "/tmp/reading.porthole" is deleted.
        """
        working = Path("/tmp/reading.porthole")
        while working.exists():
            pass

        cp, _, _ = portage_lib.split_atom_pkg(cpv)
        cat_pack = cp.split("/")
        chlogfile = "/tmp/" + cat_pack[0] + "_" + cat_pack[1] + ".porthole"
        try:
            with open(chlogfile) as f:
                file = f.readlines()
        except IOError as e:
            return "Error: %s" % e.strerror

        return file

    def set_text(self, text):
        logger.debug("ChangeLogView: set_text(); len(text) = %d", len(text))
        mydict = {}
        self.buffer.set_text('')
        if not text:
            return
        lines = text.split('\n')
        while lines[0].startswith('#'):
            self.append(lines[0], "header")
            self.nl()
            lines = lines[1:]
        for line in lines:
            found = None
            x = -1
            while not found and x < len(self.re_list) - 1:
                if line == '\n':
                    self.nl()
                    break
                x += 1
                found = (self.re_list[x]).match(line.strip())
                # debug.dprint("ChangeLogView: set_text(), checking for: %s" %self.re_fn[x])
            if found and found.groupdict():
                mydict = found.groupdict()
                # debug.dprint("ChangeLogView: set_text(), mydict = %s" %str(mydict))
            # process the parts
            getattr(self, '_%s_' % self.re_fn[x])(mydict)
        return

    def _new_ver_(self, parts):
        # debug.dprint("ChangeLogView: _new_ver_(), parts = %s" %str(parts))
        self.append(parts['atom'], 'new_ver')
        self.append_date(parts['date'])
        self.nl()
        return

    def _update_(self, parts):
        # debug.dprint("ChangeLogView: _update_(), parts = %s" %str(parts))
        self.append_date(parts['date'], self.indent)
        self.append(' ')
        self.append_developer(parts['developer'])
        self.append(' ')
        if self.atom_re.match(parts['text']):
            self.append_atom(parts['text'])
        else:
            self.append(parts['text'])
        self.nl()

    def _update2_(self, parts):
        # debug.dprint("ChangeLogView: _update2_(), parts = %s" %str(parts))
        self.append_date(parts['date'], self.indent)
        self.append(' ')
        self.append_developer(parts['developer'])
        self.nl()
        return

    def _all_(self, parts):
        # debug.dprint("ChangeLogView: _all_(), parts = %s" %str(parts))
        words = parts['text'].split()
        self.append(self.indent[:-1])
        # debug.dprint("ChangeLogView: _all_(), words = %s" %str(words))
        for word in words:
            self.append(' ')  # spacer between words
            found = None
            x = -1
            while not found and x < len(self.word_re) - 1:
                x += 1
                found = (self.word_re[x]).search(word)
                # debug.dprint("ChangeLogView: _all_(), checking for: %s" %self.word_fn[x])
            if found and found.group():
                # debug.dprint("ChangeLogView: _all_(), found = %s, %s" %(self.word_fn[x], found.group()))
                # process the parts
                getattr(self, 'append_%s' % self.word_fn[x])(word)
            else:
                # debug.dprint("ChangeLogView: _all_(), adding as text: " + word)
                self.append(word, 'normal')
        self.nl()
        return
